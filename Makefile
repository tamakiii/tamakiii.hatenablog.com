branch ?= $(shell git branch | sed -n '/\* /s///p')
hash ?= $(shell echo -n $(branch) | md5sum | awk '{ print $$1 }')
year ?= $(shell date '+%Y')
editor := vim

entry:
	make $(year)/$(branch)

$(year)/$(branch):
	mkdir -p $(year)/$(branch)
	make $(year)/$(branch)/README.md

$(year)/$(branch)/README.md:
	echo "# $(branch)" > $(@D)/$(@F)
	echo "- http://tamakiii.hatenablog.com/entry/$(branch)" >> $(@D)/$(@F)

edit:
	cd $(year)/$(branch) && $(editor) .
