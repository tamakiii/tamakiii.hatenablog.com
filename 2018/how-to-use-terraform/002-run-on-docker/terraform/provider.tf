terraform {
  backend "s3" {
    region = "ap-northeast-1"
    profile = "tamakiii"
    bucket = "terraform.tamakiii.com"
    key = "tamakiii.hatenablog.com/2018/how-to-use-terraform/002-run-on-docker/terraform.tfstate"
  }
}

provider "aws" {
  version = "~> 1.14"
  region = "${var.region}"
  profile = "${var.profile}"
}
