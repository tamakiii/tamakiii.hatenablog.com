data "aws_subnet" "left" {
  vpc_id = "${data.aws_vpc.main.id}"

  filter {
    name = "tag:Name"
    values = ["${var.name}-left"]
  }
}

data "aws_subnet" "right" {
  vpc_id = "${data.aws_vpc.main.id}"

  filter {
    name = "tag:Name"
    values = ["${var.name}-right"]
  }
}
