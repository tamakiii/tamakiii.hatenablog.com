# 001-run-on-host

## How to use
~~~sh
make install
bin/terraform --version
bin/terraform init terraform
bin/terraform plan -var-file=terraform/config/config.hcl -out=terraform/terraform.tfplan terraform
bin/terraform apply terraform/terraform.tfplan
bin/terraform destroy -var-file=terraform/config/config.hcl terraform
~~~
