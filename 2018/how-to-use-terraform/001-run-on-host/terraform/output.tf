output "vpc_main" {
  value = "${aws_vpc.main.id}"
}

output "subnet_left" {
  value = "${aws_subnet.left.id}"
}
output "subnet_right" {
  value = "${aws_subnet.right.id}"
}
