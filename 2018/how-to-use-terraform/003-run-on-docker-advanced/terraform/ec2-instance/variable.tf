variable region {
  type = "string"
}
variable profile {
  type = "string"
}

variable name {
  type = "string"
}
