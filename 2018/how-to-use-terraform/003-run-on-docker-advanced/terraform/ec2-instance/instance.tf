resource "aws_instance" "test" {
  ami                  = "ami-a77c30c1"
  instance_type        = "t2.micro"
  availability_zone    = "${var.region}a"
  vpc_security_group_ids = []

  subnet_id            = "${data.aws_subnet.left.id}"
  # key_name             = ""
  # user_data            = ""
  # iam_instance_profile = ""

  associate_public_ip_address          = false
  instance_initiated_shutdown_behavior = "terminate"

  tags {
    Name = "${var.name}-test"
  }
}
