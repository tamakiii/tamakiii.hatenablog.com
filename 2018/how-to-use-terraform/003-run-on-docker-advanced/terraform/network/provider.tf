terraform {
  backend "s3" {
    region = "ap-northeast-1"
    profile = "tamakiii"
    bucket = "terraform.tamakiii.com"
    key = "tamakiii.hatenablog.com/2018/how-to-use-terraform/003-run-on-docker-advanced/network/terraform.tfstate"
  }
}

provider "aws" {
  version = "~> 1.14"
  region = "${var.region}"
  profile = "${var.profile}"
}
