variable region {
  type = "string"
}
variable profile {
  type = "string"
}

variable cidr_block {
  type = "string"
}
variable cidr_blocks {
  type = "map"
}

variable name {
  type = "string"
}
