resource "aws_subnet" "left" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${var.region}a"
  cidr_block = "${var.cidr_blocks["left"]}"

  tags {
    Name = "${var.name}-left"
  }
}
resource "aws_subnet" "right" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${var.region}c"
  cidr_block = "${var.cidr_blocks["right"]}"

  tags {
    Name = "${var.name}-right"
  }
}
